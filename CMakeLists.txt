cmake_minimum_required(VERSION 3.16)
project(HexagonGrid)

set(CMAKE_CXX_STANDARD 14)

link_directories(include/SOIL STATIC include)

add_library(SOIL STATIC
        include/SOIL.c
        include/image_DXT.c
        include/image_helper.c
        include/stb_image_aug.c
        )

add_executable(HexagonGrid include/SOIL.c
        include/image_DXT.c
        include/image_helper.c
        include/stb_image_aug.c main.cpp Texture.cpp Texture.h HexTile.cpp HexTile.h RenderEngine.cpp RenderEngine.h Terrain.cpp Terrain.h enums/HexOrientation.h Hexagonal.cpp Hexagonal.h Point.cpp Point.h enums/SharpVertex.h enums/FlatVertex.h Drawable.h enums/TileType.h Camera.cpp Camera.h Scene.cpp Scene.h HexTileState.h enums/Input.h enums/State.h HexTileState.cpp)

target_link_libraries(HexagonGrid -lfreeglut -lopengl32 -lglu32 -lglew32 -lirrKlang)