#ifndef HEXAGONGRID_RENDERENGINE_H
#define HEXAGONGRID_RENDERENGINE_H

#include <vector>
#include "Texture.h"
#include <GL/glut.h>
#include "Terrain.h"
#include "Camera.h"

class RenderEngine {
public:
    static void resize(int width, int height);

    static void drawTexture(Drawable *drawable);

    static void display();

    static void init();

    static void timer(int value);

    static void select(GLint hits, GLuint buffer[]);

    static void mouseCallback(int button, int state, int x, int y);

    static void keyboardUpCallback(int key, int x, int y);

    static void keyboardCallback(int key, int x, int y);

    static void keyboardHandler(unsigned char key, int x, int y);

    static void keyboardUpHandler(unsigned char key, int x, int y);

    Terrain *getTerrain() const;

private:
    static Terrain *terrain_;
    static Camera *camera_;
    std::vector<Texture *> textures_;


};

#endif //HEXAGONGRID_RENDERENGINE_H
