#ifndef HEXAGONGRID_HEXTILE_H
#define HEXAGONGRID_HEXTILE_H

#include "Texture.h"
#include "enums/HexOrientation.h"
#include "Point.h"
#include "Drawable.h"
#include "enums/TileType.h"
#include <vector>
#include <unordered_map>
#include "HexTileState.h"

class HexTile : public Drawable {
public:
    HexTile(const char* filename, float x, float y, HexOrientation orientation, TileType tileType, int id);

    float getRotation() const;

    void setRotation(float rotation);

    float getH() const;

    float getR() const;

    float getSide() const;

    int getId() const;

    TileType getTileType() const;

    HexOrientation getOrientation() const;

    const std::vector<Point *> &getPoints() const;

    bool isSelected() const;

    void setIsSelected(bool isSelected);

    void toggleSelected();

    HexTileState *getLatestState() const;

    void setLatestState(HexTileState *latestState);

    void render() override;

private:
    int id_;
    float h_;
    float r_;
    float side_;
    float rotation_;
    bool isSelected_;
    TileType tileType_;
    HexOrientation orientation_;
    std::vector<Point*> points_;
    HexTileState* latestState_;
    void calculateProperties_();

};

#endif //HEXAGONGRID_HEXTILE_H
