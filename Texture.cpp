#include "Texture.h"
#include <GL/glut.h>
#include <iostream>
#include "SOIL.h"

Texture::Texture(const char *filename) {
    this->textureFile = filename;
    this->init();
}

unsigned int Texture::getTextureId() const {
    return textureId;
}

void Texture::init() {
    glewInit();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    this->textureId = SOIL_load_OGL_texture(this->textureFile, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);

    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &this->width);
    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &this->height);

//    glGenVertexArrays(1, &vertexArrayId_);
//    glBindVertexArray(vertexArrayId_);

    // x y z s t for each vertex, where x, y, z are for the vertex position and s, t are for texture coordinates
    float VBO[12] = {
            -60.0, -70.0, 0.0,
            60.0, -70.0, 0.0,
            60.0, 70.0, 0.0,
            -60.0, 70.0, 0.0
    };

    float TBO[8] = {
            0.0, 0.0,
            1.0, 0.0,
            1.0, 1.0,
            0.0, 1.0
    };

    unsigned int EBO[4] = {0, 1, 2, 3};


    glGenBuffers(1, &vertexBufferId_);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId_);
    glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), VBO, GL_STATIC_DRAW);

    glGenBuffers(1, &textureBufferId_);
    glBindBuffer(GL_ARRAY_BUFFER, textureBufferId_);
    glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(float), TBO, GL_STATIC_DRAW);

    glGenBuffers(1, &elementBufferId_);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(unsigned int), EBO, GL_STATIC_DRAW);



    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
//    glBindVertexArray(0);
}

void Texture::draw() {
    glEnable(GL_TEXTURE_2D);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindTexture(GL_TEXTURE_2D, textureId);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId_);
    glVertexPointer(3, GL_FLOAT, 0, nullptr);

    glBindBuffer(GL_ARRAY_BUFFER, textureBufferId_);
    glTexCoordPointer(2, GL_FLOAT, 0, nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferId_);
    glDrawElements(GL_QUADS, sizeof(unsigned int), GL_UNSIGNED_INT, nullptr);


    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisable(GL_TEXTURE_2D);
}

int Texture::getWidth() const {
    return width;
}

int Texture::getHeight() const {
    return height;
}

GLuint Texture::getVertexArrayId() const {
    return vertexArrayId_;
}

void Texture::setVertexArrayId(GLuint vertexArrayId) {
    vertexArrayId_ = vertexArrayId;
}

GLuint Texture::getVertexBufferId() const {
    return vertexBufferId_;
}

void Texture::setVertexBufferId(GLuint vertexBufferId) {
    vertexBufferId_ = vertexBufferId;
}

GLuint Texture::getTextureBufferId() const {
    return textureBufferId_;
}

void Texture::setTextureBufferId(GLuint textureBufferId) {
    textureBufferId_ = textureBufferId;
}

GLuint Texture::getElementBufferId() const {
    return elementBufferId_;
}
