#ifndef HEXAGONGRID_TEXTURE_H
#define HEXAGONGRID_TEXTURE_H

#include <GL/glew.h>
#include <GL/glut.h>

class Texture {
public:
    Texture(const char *filename);

    void init();

    unsigned int getTextureId() const;

    int getWidth() const;

    int getHeight() const;

    void draw();

    GLuint getVertexArrayId() const;

    void setVertexArrayId(GLuint vertexArrayId);

    GLuint getVertexBufferId() const;

    void setVertexBufferId(GLuint vertexBufferId);

    GLuint getTextureBufferId() const;

    void setTextureBufferId(GLuint textureBufferId);

    GLuint getElementBufferId() const;

protected:
    unsigned int textureId;
    const char* textureFile;
    int width;
    int height;

private:
    GLuint vertexArrayId_, vertexBufferId_, textureBufferId_, elementBufferId_;
};

#endif //HEXAGONGRID_TEXTURE_H
