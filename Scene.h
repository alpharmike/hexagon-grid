#ifndef HEXAGONGRID_SCENE_H
#define HEXAGONGRID_SCENE_H

#include <GL/glut.h>

namespace Scene {
    void init();
    void display();
    void keyboardUpCallback(int key, int x, int y);
    void keyboardCallback(int key, int x, int y);
    void keyboardHandler(unsigned char key, int x, int y);
    void keyboardUpHandler(unsigned char key, int x, int y);
    void mouseCallback(int button, int state, int x, int y);
    void select(GLint hits, GLuint buffer[]);
    int main(int argc, char **argv);
}

#endif //HEXAGONGRID_SCENE_H
