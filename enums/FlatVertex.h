#ifndef HEXAGONGRID_FLATVERTEX_H
#define HEXAGONGRID_FLATVERTEX_H

enum FlatVertex {
    UpperLeft = 0,
    UpperRight = 1,
    MiddleRight = 2,
    BottomRight = 3,
    BottomLeft = 4,
    MiddleLeft = 5,
};

#endif //HEXAGONGRID_FLATVERTEX_H
