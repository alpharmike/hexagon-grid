#ifndef HEXAGONGRID_SHARPVERTEX_H
#define HEXAGONGRID_SHARPVERTEX_H


enum SharpVertex {
    T = 0,
    UR = 1,
    BR = 2,
    B = 3,
    BL = 4,
    TL = 5,
};

#endif //HEXAGONGRID_SHARPVERTEX_H
