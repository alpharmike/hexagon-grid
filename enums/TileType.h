#ifndef HEXAGONGRID_TILETYPE_H
#define HEXAGONGRID_TILETYPE_H

enum TileType {
    CAMPSITE = 0,
    HOUSE = 1,
    PETROL = 2,
    SHOP = 3,
    SKYSCRAPER_GLASS = 4,
    TRAILER_PARK = 5,
    VILLA = 6,
    VILLAGE = 7,
};

#endif //HEXAGONGRID_TILETYPE_H
