#ifndef HEXAGONGRID_HEXORIENTATION_H
#define HEXAGONGRID_HEXORIENTATION_H

enum HexOrientation {
    Sharp,
    Flat
};

#endif //HEXAGONGRID_HEXORIENTATION_H
