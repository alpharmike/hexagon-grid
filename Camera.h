#ifndef HEXAGONGRID_CAMERA_H
#define HEXAGONGRID_CAMERA_H

#include <GL\glut.h>

class Camera {
public:
    Camera();
    Camera(float cameraTheta, float fov, float pitch, float x, float y, float z);
    Camera(int w, int h, float cameraTheta, float fov, float pitch, float x, float y, float z);

    void toggleLeftRotation() { rotatingLeft_ = !rotatingLeft_; }

    void toggleRightRotation() { rotatingRight_ = !rotatingRight_; }

    void toggleZoomingIn() { zoomingIn_ = !zoomingIn_; }

    void toggleZoomingOut() { zoomingOut_ = !zoomingOut_; }

    void updateCameraConfig(int w, int h, float cameraTheta, float fov, float pitch, float x, float y, float z);

    int getWidth() const;

    void setWidth(int width);

    int getHeight() const;

    void setHeight(int height);

    float getCameraTheta() const;

    void setCameraTheta(float cameraTheta);

    float getFov() const;

    void setFov(float fov);

    float getPitch() const;

    void setPitch(float pitch);

    bool isRotatingLeft() const;

    void setRotatingLeft(bool rotatingLeft);

    bool isRotatingRight() const;

    void setRotatingRight(bool rotatingRight);

    bool isZoomingIn() const;

    void setZoomingIn(bool zoomingIn);

    bool isZoomingOut() const;

    void setZoomingOut(bool zoomingOut);

    bool isIncreasingPitch() const;

    void setIncreasingPitch(bool increasingPitch);

    bool isDecreasingPitch() const;

    void setDecreasingPitch(bool decreasingPitch);

    float getPositionX() const;

    float getPositionY() const;

    float getPositionZ() const;

    void setPositionX(float positionX);

    void setPositionY(float positionY);

    void setPositionZ(float positionZ);

    bool isMovingRight() const;

    void setMovingRight(bool movingRight);

    bool isMovingLeft() const;

    void setMovingLeft(bool movingLeft);

    bool isIncreasingAltitude() const;

    void setIncreasingAltitude(bool increasingAltitude);

    bool isDecreasingAltitude() const;

    void setDecreasingAltitude(bool decreasingAltitude);

private:
    int width_;
    int height_;
    float cameraTheta_;
    float fov_;
    float pitch_; // relative to x axis
    float positionX_, positionY_, positionZ_;
    bool rotatingLeft_;
    bool rotatingRight_;
    bool zoomingIn_;
    bool zoomingOut_;
    bool increasingPitch_;
    bool decreasingPitch_;
    bool movingRight_;
    bool movingLeft_;
    bool increasingAltitude_;
    bool decreasingAltitude_;

};

#endif //HEXAGONGRID_CAMERA_H
