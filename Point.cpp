#include "Point.h"

Point::Point(float x, float y) {
    this->x_ = x;
    this->y_ = y;
}

float Point::getX() const {
    return x_;
}

void Point::setX(float x) {
    x_ = x;
}

float Point::getY() const {
    return y_;
}

void Point::setY(float y) {
    y_ = y;
}
