#include "RenderEngine.h"
#include "Scene.h"
#include "Camera.h"
#include <iostream>

int Scene::main(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Terrain");

    glutDisplayFunc(RenderEngine::display);
    glutIdleFunc(RenderEngine::display);
    glutReshapeFunc(RenderEngine::resize);
    glutSpecialFunc(RenderEngine::keyboardCallback);
    glutSpecialUpFunc(RenderEngine::keyboardUpCallback);
    glutKeyboardFunc(RenderEngine::keyboardHandler);
    glutKeyboardUpFunc(RenderEngine::keyboardUpHandler);
    glutMouseFunc(RenderEngine::mouseCallback);
    glutTimerFunc(25, RenderEngine::timer, 0);

    RenderEngine::init();


    glutMainLoop();

    return 0;
}
