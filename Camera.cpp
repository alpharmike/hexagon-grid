#include "Camera.h"


Camera::Camera() {
    rotatingLeft_ = false;
    rotatingRight_ = false;
    zoomingIn_ = false;
    zoomingOut_ = false;

    // set default values
    this->fov_ = 100.0;
    this->cameraTheta_ = 0.0;
    this->pitch_ = 0.0;
    this->positionX_ = 500;
    this->positionY_ = -500;
    this->positionZ_ = 1200;
}

Camera::Camera(float camera_theta, float fov, float pitch, float x, float y, float z) : Camera() {
    this->cameraTheta_ = camera_theta;
    this->fov_ = fov;
    this->pitch_ = pitch;
    this->positionX_ = x;
    this->positionY_ = y;
    this->positionZ_ = z;

    rotatingLeft_ = false;
    rotatingRight_ = false;
    zoomingIn_ = false;
    zoomingOut_ = false;

}

Camera::Camera(int w, int h, float cameraTheta, float fov, float pitch, float x, float y, float z) : Camera() {
    this->width_ = w;
    this->height_ = h;
    this->cameraTheta_ = cameraTheta;
    this->fov_ = fov;
    this->pitch_ = pitch;
    this->positionX_ = x;
    this->positionY_ = y;
    this->positionZ_ = z;

    rotatingLeft_ = false;
    rotatingRight_ = false;
    zoomingIn_ = false;
    zoomingOut_ = false;

}

void Camera::updateCameraConfig(int w, int h, float cameraTheta, float fov, float pitch, float x, float y, float z) {
    /**
     * @details: setting perspective camera configurations
     * @param w: specifies the width of the window
     * @param h: specifies the height of the window
     * @param camera_theta: specifies the rotation angle of the camera
     * @param fov: specifies the field of view angle
     */
    this->width_ = w;
    this->height_ = h;
    this->cameraTheta_ = cameraTheta;
    this->pitch_ = pitch;
    this->fov_ = fov;
    this->positionX_ = x;
    this->positionY_ = y;
    this->positionZ_ = z;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();


    gluPerspective(fov, (float) w / (float) h, 1.0, 10000.0);


    gluLookAt(
            x, y, z,
            x, y, 0,
            0, 1, 0);
}

int Camera::getWidth() const {
    return width_;
}

void Camera::setWidth(int width) {
    width_ = width;
}

int Camera::getHeight() const {
    return height_;
}

void Camera::setHeight(int height) {
    height_ = height;
}

float Camera::getCameraTheta() const {
    return cameraTheta_;
}

void Camera::setCameraTheta(float cameraTheta) {
    cameraTheta_ = cameraTheta;
}

float Camera::getFov() const {
    return fov_;
}

void Camera::setFov(float fov) {
    fov_ = fov;
}

bool Camera::isRotatingLeft() const {
    return rotatingLeft_;
}

void Camera::setRotatingLeft(bool rotatingLeft) {
    rotatingLeft_ = rotatingLeft;
}

bool Camera::isRotatingRight() const {
    return rotatingRight_;
}

void Camera::setRotatingRight(bool rotatingRight) {
    rotatingRight_ = rotatingRight;
}

bool Camera::isZoomingIn() const {
    return zoomingIn_;
}

void Camera::setZoomingIn(bool zoomingIn) {
    zoomingIn_ = zoomingIn;
}

bool Camera::isZoomingOut() const {
    return zoomingOut_;
}

void Camera::setZoomingOut(bool zoomingOut) {
    zoomingOut_ = zoomingOut;
}

float Camera::getPitch() const {
    return pitch_;
}

void Camera::setPitch(float pitch) {
    pitch_ = pitch;
}

bool Camera::isIncreasingPitch() const {
    return increasingPitch_;
}

void Camera::setIncreasingPitch(bool increasingPitch) {
    increasingPitch_ = increasingPitch;
}

bool Camera::isDecreasingPitch() const {
    return decreasingPitch_;
}

void Camera::setDecreasingPitch(bool decreasingPitch) {
    decreasingPitch_ = decreasingPitch;
}

float Camera::getPositionX() const {
    return positionX_;
}

float Camera::getPositionY() const {
    return positionY_;
}

float Camera::getPositionZ() const {
    return positionZ_;
}

void Camera::setPositionX(float positionX) {
    positionX_ = positionX;
}

void Camera::setPositionY(float positionY) {
    positionY_ = positionY;
}

void Camera::setPositionZ(float positionZ) {
    positionZ_ = positionZ;
}

bool Camera::isMovingRight() const {
    return movingRight_;
}

void Camera::setMovingRight(bool movingRight) {
    movingRight_ = movingRight;
}

bool Camera::isMovingLeft() const {
    return movingLeft_;
}

void Camera::setMovingLeft(bool movingLeft) {
    movingLeft_ = movingLeft;
}

bool Camera::isIncreasingAltitude() const {
    return increasingAltitude_;
}

void Camera::setIncreasingAltitude(bool increasingAltitude) {
    increasingAltitude_ = increasingAltitude;
}

bool Camera::isDecreasingAltitude() const {
    return decreasingAltitude_;
}

void Camera::setDecreasingAltitude(bool decreasingAltitude) {
    decreasingAltitude_ = decreasingAltitude;
}



