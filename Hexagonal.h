#ifndef HEXAGONGRID_HEXAGONAL_H
#define HEXAGONGRID_HEXAGONAL_H

#include <cmath>

class Hexagonal {
public:
    static double calculateSide(int width);

    static double calculateR(int width);

    static double calculateH(int width, int height);


};

#endif //HEXAGONGRID_HEXAGONAL_H
