#include "HexTile.h"
#include "Hexagonal.h"
#include <iostream>

HexTile::HexTile(const char *filename, float x, float y, HexOrientation orientation, TileType tileType, int id) : Drawable(filename, x, y, id) {
    this->x_ = x;
    this->y_ = y;
    this->orientation_ = orientation;
    this->tileType_ = tileType;
    this->id_ = id;
    this->isSelected_ = false;
    latestState_ = new HexTileState(this, State::TILE_UNSELECTED);
    this->calculateProperties_();
}

float HexTile::getH() const {
    return h_;
}

float HexTile::getR() const {
    return r_;
}

float HexTile::getRotation() const {
    return rotation_;
}

void HexTile::setRotation(float rotation) {
    rotation_ = rotation;
}

float HexTile::getSide() const {
    return side_;
}

int HexTile::getId() const {
    return id_;
}

TileType HexTile::getTileType() const {
    return tileType_;
}

HexOrientation HexTile::getOrientation() const {
    return orientation_;
}

const std::vector<Point *> &HexTile::getPoints() const {
    return points_;
}

void HexTile::calculateProperties_() {
    int width = texture_->getWidth();
    int height  = texture_->getHeight();
//    std::cout << width << " " << height << "\n";
    this->h_ = Hexagonal::calculateH(width, height);
    this->r_ = Hexagonal::calculateR(width);
    this->side_ = Hexagonal::calculateSide(width);
//    std::cout << r_ << " " << side_ << " " << h_ << "\n";
    Point *point1, *point2, *point3, *point4, *point5, *point6;
    switch (orientation_) {
        case HexOrientation::Flat:
            // x,y coordinates are top left point
            point1 = new Point(x_, y_);
            point2 = new Point(x_ + side_, y_);
            point3 = new Point(x_ + side_ + h_, y_ + r_);
            point4 = new Point(x_ + side_, y_ + (float)width);
            point5 = new Point(x_, y_ + (float)width);
            point6 = new Point(x_ - h_, y_ + r_);
            this->points_.push_back(point1);
            this->points_.push_back(point2);
            this->points_.push_back(point3);
            this->points_.push_back(point4);
            this->points_.push_back(point5);
            this->points_.push_back(point6);
            break;
        case HexOrientation::Sharp:
            //x,y coordinates are top center point
            point1 = new Point(x_, y_);
            point2 = new Point(x_ + r_, y_ + h_); // BR
            point3 = new Point(x_ + r_, y_ + side_ + h_); // UR
            point4 = new Point(x_, y_ + (float )height);
            point5 = new Point(x_ - r_, y_ + side_ + h_);
            point6 = new Point(x_ - r_, y_ + h_);
            this->points_.push_back(point1);
            this->points_.push_back(point2);
            this->points_.push_back(point3);
            this->points_.push_back(point4);
            this->points_.push_back(point5);
            this->points_.push_back(point6);
            break;
        default:
            break;
    }
}

bool HexTile::isSelected() const {
    return isSelected_;
}

void HexTile::setIsSelected(bool isSelected) {
    isSelected_ = isSelected;
}

void HexTile::toggleSelected() {
    this->isSelected_ = !this->isSelected_;

}

void HexTile::render() {
    glLoadName(id_);
    latestState_->render();
}

HexTileState *HexTile::getLatestState() const {
    return latestState_;
}

void HexTile::setLatestState(HexTileState *latestState) {
    latestState_ = latestState;
}
