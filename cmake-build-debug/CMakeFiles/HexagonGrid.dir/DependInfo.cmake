# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "D:/OpenGLProjects/HexagonGrid/include/SOIL.c" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/include/SOIL.c.obj"
  "D:/OpenGLProjects/HexagonGrid/include/image_DXT.c" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/include/image_DXT.c.obj"
  "D:/OpenGLProjects/HexagonGrid/include/image_helper.c" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/include/image_helper.c.obj"
  "D:/OpenGLProjects/HexagonGrid/include/stb_image_aug.c" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/include/stb_image_aug.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/OpenGLProjects/HexagonGrid/Camera.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Camera.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/HexTile.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/HexTile.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/HexTileState.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/HexTileState.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/Hexagonal.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Hexagonal.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/Point.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Point.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/RenderEngine.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/RenderEngine.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/Scene.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Scene.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/Terrain.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Terrain.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/Texture.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/Texture.cpp.obj"
  "D:/OpenGLProjects/HexagonGrid/main.cpp" "D:/OpenGLProjects/HexagonGrid/cmake-build-debug/CMakeFiles/HexagonGrid.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
