#ifndef HEXAGONGRID_HEXTILESTATE_H
#define HEXAGONGRID_HEXTILESTATE_H

#include "enums/Input.h"
#include "enums/State.h"

class HexTile;

class HexTileState {
public:
    HexTileState(HexTile *hexTile, State tileState) : hexTile_(hexTile), tileState_(tileState) {}

    void render();
    void handleInput(Input input);

    HexTile *getHexTile() const;

    void setHexTile(HexTile *hexTile);

    State getTileState() const;

    void setTileState(State tileState);

private:
    HexTile* hexTile_;
    State tileState_;
};

#endif //HEXAGONGRID_HEXTILESTATE_H
