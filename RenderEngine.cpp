#include "iostream"
#include "RenderEngine.h"

#define BUFFER_CAPACITY 4096

Camera* RenderEngine::camera_ = nullptr;
Terrain* RenderEngine::terrain_  = nullptr;

void RenderEngine::init() {
    RenderEngine::terrain_ = new Terrain(50, 50, 0, 0, HexOrientation::Sharp);
    RenderEngine::camera_ = new Camera(0.0, 70.0, 10.0, 1500, 1000, 1500);
}

void RenderEngine::resize(int width, int height) {
    RenderEngine::camera_->updateCameraConfig(width, height, camera_->getCameraTheta(), camera_->getFov(), camera_->getPitch(),
                                camera_->getPositionX(), camera_->getPositionY(), camera_->getPositionZ());
}

void RenderEngine::display() {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < RenderEngine::terrain_->getHeight(); ++i) {
        for (int j = 0; j < RenderEngine::terrain_->getWidth() ; ++j) {
            HexTile* hexTile = RenderEngine::terrain_->getTiles()[i][j];
            hexTile->render();
        }
    }

    glutSwapBuffers();
}

void RenderEngine::timer(int value) {
    float fov = camera_->getFov();
    float theta = camera_->getCameraTheta();
    float pitch = camera_->getPitch();
    float x = camera_->getPositionX();
    float y = camera_->getPositionY();
    float z = camera_->getPositionZ();

    if (camera_->isZoomingIn()) {
        if (fov >= 0)
            fov -= 1;
    } else if (camera_->isZoomingOut()) {
        if (fov <= 200)
            fov += 1;
    }

    if (camera_->isRotatingLeft()) {
        theta += 1;
    } else if (camera_->isRotatingRight()) {
        theta -= 1;
    }

    if (camera_->isIncreasingPitch()) {
        pitch += 1;
        y += 10;
    } else if (camera_->isDecreasingPitch()) {
        pitch -= 1;
        y -= 10;
    }

    if (camera_->isMovingLeft()) {
        x -= 10.0;
    } else if (camera_->isMovingRight()) {
        x += 10.0;
    }

    if (camera_->isIncreasingAltitude()) {
        z += 10.0;
    } else if (camera_->isDecreasingAltitude()) {
        z -= 10.0;
    }

    auto width = glutGet(GLUT_WINDOW_WIDTH);
    auto height = glutGet(GLUT_WINDOW_HEIGHT);

    camera_->updateCameraConfig(width, height, theta, fov, pitch, x, y, z);

    glutTimerFunc(25, RenderEngine::timer, value + 1);
    glutPostRedisplay();
}

void RenderEngine::keyboardUpCallback(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_UP:
            camera_->setZoomingIn(false);
            break;

        case GLUT_KEY_DOWN:
            camera_->setZoomingOut(false);
            break;

        case GLUT_KEY_LEFT:
            camera_->setRotatingLeft(false);
            break;

        case GLUT_KEY_RIGHT:
            camera_->setRotatingRight(false);
            break;

        default:
            break;
    }
}

void RenderEngine::keyboardCallback(int key, int x, int y) {
    switch (key) {
        case GLUT_KEY_UP:
            camera_->setZoomingIn(true);
            break;

        case GLUT_KEY_DOWN:
            camera_->setZoomingOut(true);
            break;

        case GLUT_KEY_LEFT:
            camera_->setRotatingLeft(true);
            break;

        case GLUT_KEY_RIGHT:
            camera_->setRotatingRight(true);
            break;

        default:
            break;
    }
}

void RenderEngine::keyboardHandler(unsigned char key, int x, int y) {
    switch (key) {
        case 'W':
        case 'w':
            camera_->setIncreasingPitch(true);
            break;
        case 'S':
        case 's':
            camera_->setDecreasingPitch(true);
            break;
        case 'A':
        case 'a':
            camera_->setMovingLeft(true);
            break;
        case 'D':
        case 'd':
            camera_->setMovingRight(true);
            break;
        case 'E':
        case 'e':
            camera_->setIncreasingAltitude(true);
            break;
        case 'Q':
        case 'q':
            camera_->setDecreasingAltitude(true);
            break;
        default:
            break;

    }
}

void RenderEngine::keyboardUpHandler(unsigned char key, int x, int y) {
    switch (key) {
        case 'W':
        case 'w':
            camera_->setIncreasingPitch(false);
            break;
        case 'S':
        case 's':
            camera_->setDecreasingPitch(false);
            break;
        case 'A':
        case 'a':
            camera_->setMovingLeft(false);
            break;
        case 'D':
        case 'd':
            camera_->setMovingRight(false);
            break;
        case 'E':
        case 'e':
            camera_->setIncreasingAltitude(false);
            break;
        case 'Q':
        case 'q':
            camera_->setDecreasingAltitude(false);
            break;
        default:
            break;

    }
}

void RenderEngine::mouseCallback(int button, int state, int x, int y) {
    GLuint selectBuf[BUFFER_CAPACITY];
    GLint hits;
    GLint viewport[4];
    if (button != GLUT_LEFT_BUTTON || state != GLUT_DOWN)
        return;

    glGetIntegerv(GL_VIEWPORT, viewport);
    glSelectBuffer(BUFFER_CAPACITY, selectBuf);
    glRenderMode(GL_SELECT);

    glInitNames();
    glPushName(-1);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 0.05, 0.05, viewport);

    auto w = glutGet(GLUT_WINDOW_WIDTH);
    auto h = glutGet(GLUT_WINDOW_HEIGHT);


    gluPerspective(camera_->getFov(), (float) w / (float) h, 1.0, 10000.0);

    gluLookAt(camera_->getPositionX(), camera_->getPositionY(), camera_->getPositionZ(), camera_->getPositionX(),
              camera_->getPositionY(), 0, 0, 1, 0);

    glMatrixMode(GL_MODELVIEW);

    display();

    glLoadName(-1);


    glPopMatrix();

    glutSwapBuffers();
    // // Process Hits
    hits = glRenderMode(GL_RENDER);
    select(hits, selectBuf);
}

void RenderEngine::select(GLint hits, GLuint buffer[]) {
    // Process hits
    if (hits == 0) {
        return;
    }
    unsigned int i, j;
    GLuint names, *ptr, minZ, *ptrNames;

    ptr = (GLuint *) buffer;
    minZ = 0xffffffff;
    for (i = 0; i < hits; i++) {
        names = *ptr;
        ptr++;
        if (*ptr < minZ) {
            minZ = *ptr;
            ptrNames = ptr + 2;
        }
        ptr += names + 2;
    }
    ptr = ptrNames;


    for (const std::vector<HexTile *> row : terrain_->getTiles()) {
        for (HexTile *tile : row) {
            if (tile->getId() == *ptr) {
                // CHECK
                tile->getLatestState()->handleInput(LMB_DOWN);
            }
        }
    }


    std::cout << *ptr << std::endl;
}

Terrain *RenderEngine::getTerrain() const {
    return terrain_;
}

