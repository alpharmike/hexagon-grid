#include "Hexagonal.h"

double Hexagonal::calculateSide(int width) {
    auto r = width / 2;
    float radian = 30.0 * M_PI / 180;
    return r / cos(radian);
}

double Hexagonal::calculateR(int width) {
    return width / 2;
}

double Hexagonal::calculateH(int width, int height) {
    double side = Hexagonal::calculateSide(width);
    return (height - side) / 2;
}


