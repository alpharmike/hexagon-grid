#ifndef HEXAGONGRID_TERRAIN_H
#define HEXAGONGRID_TERRAIN_H

#include <vector>
#include "HexTile.h"
#include "enums/HexOrientation.h"


class Terrain {
public:
    Terrain(int width, int height, float xOffset, float yOffset, HexOrientation orientation);

    const std::vector<std::vector<HexTile *>> &getTiles() const;

    int getWidth() const;

    int getHeight() const;



private:
    int width_, height_;
    float xOffset_, yOffset_;
    HexOrientation orientation_;
    std::vector<std::vector<HexTile*>> tiles_;
    std::unordered_map<TileType, const char*> tileFiles_;
    void initTileFiles_();
    void init_();


};

#endif //HEXAGONGRID_TERRAIN_H
