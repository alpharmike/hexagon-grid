#ifndef HEXAGONGRID_POINT_H
#define HEXAGONGRID_POINT_H

class Point {
public:
    Point(float x, float y);

    float getX() const;

    void setX(float x);

    float getY() const;

    void setY(float y);

private:
    float x_, y_;
};

#endif //HEXAGONGRID_POINT_H
