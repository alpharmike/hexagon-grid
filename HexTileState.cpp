#include "HexTile.h"
#include <GL/glut.h>
#include "HexTileState.h"


void HexTileState::render() {
    glPushMatrix();
    glTranslatef(hexTile_->getX(), hexTile_->getY(), 0.0);
    if (tileState_ == TILE_SELECTED) {
        glScalef(0.7, 0.7, 1.0);
    }
    hexTile_->getTexture()->draw();
    glPopMatrix();
}

void HexTileState::handleInput(Input input) {
    if (input == LMB_DOWN) {
        if (tileState_ == TILE_UNSELECTED) {
            tileState_ = TILE_SELECTED;
        } else {
            tileState_ = TILE_UNSELECTED;
        }
    }
}

HexTile *HexTileState::getHexTile() const {
    return hexTile_;
}

void HexTileState::setHexTile(HexTile *hexTile) {
    hexTile_ = hexTile;
}

State HexTileState::getTileState() const {
    return tileState_;
}

void HexTileState::setTileState(State tileState) {
    tileState_ = tileState;
}
