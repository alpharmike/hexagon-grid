#ifndef HEXAGONGRID_DRAWABLE_H
#define HEXAGONGRID_DRAWABLE_H

#include "Texture.h"
#include <GL/glut.h>
#include <unordered_map>

class Drawable {
public:
    Drawable() {};
    Drawable(const char* filename,  float x, float y, int id) {
        this->x_ = x;
        this->y_ = y;
        this->id_ = id;
        this->assignTexture(filename);
    }

    Texture *getTexture() const {
        return texture_;
    }

    void setTexture(Texture *texture) {
        texture_ = texture;
    }

    float getX() const {
        return x_;
    }

    void setX(float x) {
        x_ = x;
    }

    float getY() const {
        return y_;
    }

    void setY(float y) {
        y_ = y;
    }

    int getId() const {
        return id_;
    }

    void setId(int id) {
        id_ = id;
    }

    virtual void render() = 0;

    void assignTexture(const char* filename) {
        // Using a flyweight pattern to avoid creating already loaded textures

        if (loadedTextures_.find(filename) == loadedTextures_.end()) {
            loadedTextures_[filename] = new Texture(filename);
        }

        this->texture_ = loadedTextures_[filename];
    }

protected:
    int id_;
    Texture* texture_;
    float x_, y_;
    std::unordered_map<const char*, Texture*> loadedTextures_;

};

#endif //HEXAGONGRID_DRAWABLE_H
