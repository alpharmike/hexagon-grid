#include "Terrain.h"
#include "Hexagonal.h"
#include "enums/FlatVertex.h"
#include "enums/SharpVertex.h"
#include <iostream>
#include <ctime>
#include <random>
#include "enums/TileType.h"

using namespace std;

Terrain::Terrain(int width, int height, float xOffset, float yOffset, HexOrientation orientation) {
    this->width_ = width;
    this->height_ = height;
    this->xOffset_ = xOffset;
    this->yOffset_ = yOffset;
    this->orientation_ = orientation;

    this->init_();

}

void Terrain::init_() {
    this->initTileFiles_();
    bool inTopRow = false;
    bool inBottomRow = false;
    bool inLeftColumn = false;
    bool inRightColumn = false;
    bool isTopLeft = false;
    bool isTopRight = false;
    bool isBottomLeft = false;
    bool isBottomRight = false;
    float h = Hexagonal::calculateH(120, 140);
    float r = Hexagonal::calculateR(120);
    float side = Hexagonal::calculateSide(120);

    srand(time(nullptr));

    int tileNumber = 0;

    for (int i = 0; i < height_; ++i) {
        std::vector<HexTile *> hexTiles;
        tiles_.push_back(hexTiles);
    }

    for (int i = 0; i < height_; i++) {
        for (int j = 0; j < width_; j++) {
            // Set position booleans

            inTopRow = i == height_ - 1;
            inBottomRow = i == 0;
            inLeftColumn = j == 0;
            inRightColumn = j == width_ - 1;
            isTopLeft = inTopRow && inLeftColumn;
            isTopRight = inTopRow && inRightColumn;
            isBottomLeft = inBottomRow && inLeftColumn;
            isBottomRight = inBottomRow && inRightColumn;

            TileType tileType = static_cast<TileType>(rand() % 8);
            const char* filename = this->tileFiles_[tileType];

            float x, y = 0;

            if (isBottomLeft) {
                x = 0;
                y = 0;
            } else if (inBottomRow) {
                y = 0;
                x = tiles_[i][j - 1]->getX() + 2 * r;
            } else if (i % 2 == 0) {
                x = tiles_[i - 1][j]->getX() - r;
                y = tiles_[i - 1][j]->getY() + h + side;
            } else if (i % 2 == 1) {
                x = tiles_[i - 1][j]->getX() + r;
                y = tiles_[i - 1][j]->getY() + h + side;
            }

            HexTile *newTile = new HexTile(filename, x, y, orientation_, tileType, tileNumber);
            this->tiles_[i].push_back(newTile);
            ++tileNumber;
        }
    }
}

const std::vector<std::vector<HexTile *>> &Terrain::getTiles() const {
    return tiles_;
}

int Terrain::getWidth() const {
    return width_;
}

int Terrain::getHeight() const {
    return height_;
}

void Terrain::initTileFiles_() {
    this->tileFiles_[TileType::CAMPSITE] = "./Assets/Tiles/Modern/modern_campsite.png";
    this->tileFiles_[TileType::HOUSE] = "./Assets/Tiles/Modern/modern_house.png";
    this->tileFiles_[TileType::PETROL] = "./Assets/Tiles/Modern/modern_petrol.png";
    this->tileFiles_[TileType::SHOP] = "./Assets/Tiles/Modern/modern_shop.png";
    this->tileFiles_[TileType::SKYSCRAPER_GLASS] = "./Assets/Tiles/Modern/modern_skyscraperGlass.png";
    this->tileFiles_[TileType::TRAILER_PARK] = "./Assets/Tiles/Modern/modern_trailerpark.png";
    this->tileFiles_[TileType::VILLA] = "./Assets/Tiles/Modern/modern_villa.png";
    this->tileFiles_[TileType::VILLAGE] = "./Assets/Tiles/Modern/modern_villageLarge.png";
}